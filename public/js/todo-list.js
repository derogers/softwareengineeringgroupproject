/* global Noty */
// CREATE TODO LIST ITEM
$('#addtodo').submit(function (e) {
    e.preventDefault() // avoid to execute the actual submit of the form.

    const form = $(this)
    const url = form.attr('action')
    // TODO: add failure scenario
    $.ajax({
        type: 'POST',
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function (data) {
            // add new list item
            $('#todo_list').append('<li class="list-group-item" todo_id=' + data._id + "><a class=\"btn btn-primary btn-tiny\" onclick=\"deleteTodoItem('" + data._id + "'); return false;\" href=\"#\" role=\"button\">✓</a>&nbsp" + data.info + '</li>')
            // clear current input
            $('#todo_item').val('')
            // show notification
            new Noty({
                layout: 'bottomLeft',
                timeout: 1500,
                theme: 'bootstrap-v4',
                type: 'info',
                text: 'Todo item created: ' + data.info
            }).show()
        }
    })
})

// DELETE TODO LIST ITEM
const deleteTodoItem = function (item) { // eslint-disable-line no-unused-vars
    // TODO: add failure scenario
    $.ajax({
        type: 'DELETE',
        url: '/delete-todo',
        data: {
            item: item
        },
        success: function (data) {
            // remove list item
            $("li[todo_id='" + item + "']").remove()
            // show notification
            new Noty({
                layout: 'bottomLeft',
                timeout: 1500,
                theme: 'bootstrap-v4',
                type: 'info',
                text: 'Todo item deleted'
            }).show()
        }
    })
}
