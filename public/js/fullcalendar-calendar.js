function renderCalendar (calendarEvents) { // eslint-disable-line no-unused-vars
    $('#calendar').fullCalendar({
        ignoreTimezone: true,
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },
        editable: false,
        firstDay: 0,
        selectable: true,
        defaultView: 'agendaWeek',
        axisFormat: 'h:mm',
        columnFormat: {
            month: 'ddd',
            week: 'ddd d',
            day: 'dddd M/d',
            agendaDay: 'dddd d'
        },
        titleFormat: {
            month: 'MMMM yyyy',
            week: 'MMMM yyyy',
            day: 'MMMM yyyy'
        },
        allDaySlot: false,
        selectHelper: false,
        droppable: false,
        events: calendarEvents,
        eventClick: function (calEvent, jsEvent, view) {
            // set fields for edit event form
            $('#eventEditTitle').val(calEvent.title)
            $('#eventEditType').val(calEvent.type)
            $('#eventEditStart').val((convertUTCDateToLocalDate(new Date(calEvent.start))).toJSON().slice(0, 19))
            $('#eventEditEnd').val((convertUTCDateToLocalDate(new Date(calEvent.end))).toJSON().slice(0, 19))
            $('#eventEditDescription').val(calEvent.description)
            $('#eventEditId').val(calEvent._id)
            $('#eventDeleteId').val(calEvent._id)
            $('#editEventModal').modal('show')
        }
    })
};

// conversion for date to local timezone
function convertUTCDateToLocalDate (date) {
    const newDate = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000)
    return newDate
}