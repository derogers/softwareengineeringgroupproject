function renderCalendar (calendarEvents) { // eslint-disable-line no-unused-vars
    console.log(calendarEvents);
    $('#calendar').fullCalendar({
        ignoreTimezone: true,
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },
        editable: false,
        firstDay: 0,
        selectable: true,
        defaultView: 'basicWeek',
        axisFormat: 'h:mm',
        columnFormat: {
            month: 'ddd',
            week: 'ddd d',
            day: 'dddd M/d',
            agendaDay: 'dddd d'
        },
        titleFormat: {
            month: 'MMMM yyyy',
            week: 'MMMM yyyy',
            day: 'MMMM yyyy'
        },
        allDaySlot: false,
        selectHelper: false,
        droppable: false,
        events: calendarEvents,
        eventClick: function (calEvent, jsEvent, view) {
            // set fields for edit(create) lesson plan form
            $('#planSubject').val(calEvent.title)
            $('#planObjective').val(calEvent.lesson.objective)
            $('#planLesson').val(calEvent.lesson.lesson)
            $('#planAssessment').val(calEvent.lesson.assessment)
            $('#eventEditId').val(calEvent._id)
            if (calEvent.lesson.state === "new") {
                $('#lessonPlanStatus').html('New')
                $("#lessonPlanStatus").attr("class","badge badge-primary");
            }
            else if (calEvent.lesson.state === "pending") {
                $('#lessonPlanStatus').html('Pending Approval')
                $("#lessonPlanStatus").attr("class","badge badge-warning");
            }
            else if (calEvent.lesson.state === "approved") {
                $('#lessonPlanStatus').html('Approved')
                $("#lessonPlanStatus").attr("class","badge badge-success");
            }
            else {
                $('#lessonPlanStatus').remove();
            }
            $('#editPlanModal').modal('show')
        }
    })
};