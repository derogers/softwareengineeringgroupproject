# AppName - Software Engineering Project

## Group Members
- Carissa Barrett
- Derek Rogers
- Adolfo Ortiz
- Anna Pikus

---

## GIT Guidelines
- Never push to `master` branch
    - Code needs to go into `master` only through merge request from `develop`
- Branch `develop` is the development/integration branch
- This way we keep `master` clean and do not break working code

---

## Environment Requirements
- Node - application web server
    - Install here: https://nodejs.org/en/download/
- Git - version control
    - Install git guide: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- npm (5.2.0+) - node package manager
    - Installed with node
- Express - application framework
    - Available as node module
- MongoDB - application database
    - Will use Mongo Cloud Atlas

---

## How do I run the application?
- Ensure node + npm are installed correctly
    - Run `node -v` or `npm -v` from command line to see currently installed versions
- Clone Project from gitlab + cd into project directory
    - Make sure SSH keys are setup to access our project
    - Run `git clone git@gitlab.com:derogerssoftwareengineeringgroupproject.git` 
    - Run `cd SoftwareEngineeringGroupProject` to change directory
- Run `npm install` to install project dependencies
    - This may take a while
    - Note: dependencies and versions are defined in the `package.json` file in the root directory of the project
- Run `npm run dev` to start the development server
    - Should see the following in the output: `Server started.  Please visit: 127.0.0.1:8080`
- Visit 127.0.0.1:8080 to see the running application

---

## How do I contribute?
1) Clone project, if not already cloned
2) Run `git checkout develop` to pull latest code
3) Implement required code changes (whatever you are working on)
4) Run `git pull` to ensure you have latest changes before commiting code
5) Run `git commit <filename> -m "Your commit message"` to make a commit
5) Run `git push` to sync your changes to gitlab remote
6) Rinse. Repeat.

Note: GIT Guide: http://rogerdudler.github.io/git-guide/

---

## Database Access
- Mongo Cloud Atlas - host for MongoDB database instance
- Management:
    - Login: https://cloud.mongodb.com/user
    - Username: carissa.meg@gmail.com
    - Password: command10
    - Database: lessonsdb

## Deployments
- We have a pipeline that runs on the master branch and automatically deploys our code to the digitalocean droplet.
- After deployment, it automatically restarts the node server on the droplet with the new changes.
    - Current Droplet URL: http://165.22.180.198:8080/