  
var middleware = {};
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')

// auth check
// TODO: CHECK ROLE
middleware.checkAuth = function (req, res, next){
    if (req.isAuthenticated()){
        return next()
    }
    req.session.redirectRoute = req.route.path
    res.redirect('/login')
}

// build common user details
middleware.buildCommonUserObject = function (req) {
    let userObject
    if (req.user) {
        userObject = {
            currentUsername: req.user.username,
            currentUserFirstName: req.user.firstname,
            currentUserLastName: req.user.lastname,
            currentUserEmail: req.user.email,
            currentUserRole: req.user.role,
            currentUserZip: req.user.zip
        }
    } else {
        userObject = {
            currentUsername: null,
            currentUserFirstName: null,
            currentUserLastName: null,
            currentUserEmail: null,
            currentUserRole: null,
            currentUserZip: null
        }
    }
    return userObject
}

module.exports = middleware;