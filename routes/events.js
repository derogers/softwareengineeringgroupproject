// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware");


//router for lesson plans and calendar events

// LESSON PLANS ---------------------------------------------------------------------------

// lessonPlan page for teacher
router.get('/lessonplans', middleware.checkAuth, async function (req, res){   
    let classEvents = await CalendarEvent.find({
        username: req.user.username,
        type: "Class"
    }, function (err) {
        if (err) {
            console.log('LESSONPLAN Class Events Error: ' + err)
        }
    }).sort({start: 'asc'}).exec()
    res.render('lessonPlans.ejs', {
        ...middleware.buildCommonUserObject(req),
        calendarEvents: classEvents
    })
})

router.post('/edit-lesson-plan', middleware.checkAuth, function (req, res){
    CalendarEvent.findOneAndUpdate(
        { _id: new mongoose.Types.ObjectId(req.body.eventEditId) }, {
            $set: {
                'lesson.objective': req.body.planObjective,
                'lesson.lesson': req.body.planLesson,
                'lesson.assessment': req.body.planAssessment
            }
        }, function (err, doc) {
            if (err) {
                console.log('Lesson plan edit error: ' + err)
            } else {
                console.log('Successfully udpated lesson plan: ' + doc)
            }
        }
    )
    res.redirect('/lessonplans')
})

router.post('/submit-plans', middleware.checkAuth, function (req, res){
    CalendarEvent.updateMany(
        {   username: req.user.username,
            type: "Class",
            start: {"$gte": req.body.planStart, "$lt": req.body.planEnd}}, {
                $set:{
                    'lesson.state': "pending"
                }
            }, function (err) {
            if (err) {
                console.log('ERROR UPDATING STATE OF LESSON PLANS ' + err)
            }
        }
    )
    res.redirect('/lessonplans')
})

// lessonPlan page for principal
router.get('/lessonPlans-principal', middleware.checkAuth, async function (req, res){
    let pendingLessonPlans = await CalendarEvent.find({"lesson.state": "pending"}).sort({start: 'asc'}).exec()
    res.render('lessonPlans-principal.ejs', {
        ...middleware.buildCommonUserObject(req),
        lessonPlans: pendingLessonPlans
    })
    console.log("all calendar events = " + pendingLessonPlans)
})

// approve lesson plan status
router.post('/approve-lesson-plan', middleware.checkAuth, function (req, res){
    CalendarEvent.findOneAndUpdate(
        { _id: new mongoose.Types.ObjectId(req.body.eventApproveId) }, {
            $set: {
                'lesson.state': 'approved'
            }
        }, function (err, doc) {
            if (err) {
                console.log('Lesson plan edit error: ' + err)
            } else {
                console.log('Successfully udpated lesson plan: ' + doc)
            }
        }
    )
    res.redirect('/lessonPlans-principal')
})

// deny lesson plan status
router.post('/deny-lesson-plan', middleware.checkAuth, function (req, res){
    CalendarEvent.findOneAndUpdate(
        { _id: new mongoose.Types.ObjectId(req.body.eventDenyId) }, {
            $set: {
                'lesson.state': 'denied'
            }
        }, function (err, doc) {
            if (err) {
                console.log('Lesson plan edit error: ' + err)
            } else {
                console.log('Successfully udpated lesson plan: ' + doc)
            }
        }
    )
    res.redirect('/lessonPlans-principal')
})

// createLessonPlan page
router.get('/createlessonplan', function (req, res){
    res.render('createlessonplan.ejs', { ...middleware.buildCommonUserObject(req) })
})

// CALENDAR EVENTS ---------------------------------------------------------------------------

// calendar events page
router.get('/calendar', middleware.checkAuth, function (req, res){
    CalendarEvent.find({
        username: req.user.username
    }, function (err, events) {
        if (err) {
            console.log('Calendar Error: ' + err)
            res.render('calendar',
                {
                    ...middleware.buildCommonUserObject(req),
                    calendarEvents: []
                })
        } else {
            res.render('calendar',
                {
                    ...middleware.buildCommonUserObject(req),
                    calendarEvents: events
                })
        }
    })
})

// delete calendar event
router.post('/delete-calendar-event', middleware.checkAuth, function (req, res){
    CalendarEvent.findOneAndDelete(
        { _id: new mongoose.Types.ObjectId(req.body.eventDeleteId) }, function (err, doc) {
            if (err) {
                console.log('Error deleting a calendar event ' + err)
            } else {
                console.log('Deleted calendar event ' + doc)
            }
        }
    )
    res.redirect('/calendar')
})

// edit calendar event
router.post('/edit-calendar-event', middleware.checkAuth, function (req, res){
    CalendarEvent.findOneAndUpdate(
        { _id: new mongoose.Types.ObjectId(req.body.eventEditId) }, {
            $set: {
                title: req.body.eventEditTitle,
                type: req.body.eventEditType,
                start: dateCST(req.body.eventEditStart),
                end: dateCST(req.body.eventEditEnd),
                description: req.body.eventEditDescription,
                className: req.body.eventEditType === 'Class' ? 'info' : 'success'
            }
        }, function (err, doc) {
            if (err) {
                console.log('Calendar event edit error: ' + err)
            } else {
                console.log('Successfully udpated calendar event: ' + doc)
            }
        }
    )
    res.redirect('/calendar')
})

// add new calendar event
router.post('/add-calendar-event', middleware.checkAuth, function (req, res){
    console.log('Created event:')
    const newEvent = new CalendarEvent({
        title: req.body.eventTitle,
        type: req.body.eventType,
        start: dateCST(req.body.eventStart),
        end: dateCST(req.body.eventEnd),
        description: req.body.eventDescription,
        username: req.user.username,
        className: req.body.eventType === 'Class' ? 'info' : 'success',
        lesson: {}
    })

    newEvent.save(function (err){
        if (err) {
            console.log('Error creating event: ' + err)
        } else {
            console.log('Event Created')
            console.log(newEvent)
        }
    })

    res.redirect('/calendar')
})

// convert date to CST
function dateCST(date) {
    return moment.utc(date).tz('America/Chicago').format()
}



module.exports = router;