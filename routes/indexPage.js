// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware");

router.get('/', function (req, res){
    res.render('index', { ...middleware.buildCommonUserObject(req) })
})

module.exports = router;