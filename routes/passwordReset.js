// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('../models/user')
CalendarEvent = require('../models/calendarEvent')
Todo = require('../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("../middleware");

var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var bcrypt = require('bcrypt-nodejs');

router.get('/reset/:token', function(req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
      if (!user) {
        //req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/forgot');
      }
      res.render('reset', {
        user: req.user
      });
    });
  });

  router.post('/reset/:token', function(req, res) {
      console.log("here");
    async.waterfall([
      function(done) {
        User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
          if (!user) {
              console.log("here2");
            //req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('back');
          }
         
            user.setPassword(req.body.password, function(err,user){
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function(err) {
              
                done(err, user);
              
            });
        });
          
          console.log('password ' + user.password  + 'and the user is' + user)
          console.log("here3")
       

        });
      },
      function(user, done) {
        console.log("here4")
        var smtpTransport = nodemailer.createTransport( {
          service: 'gmail',
          auth: {
              user: 'planithelpdesk404@gmail.com',
              pass: 'planitearth'
          }
        });
        var mailOptions = {
          to: user.email,
          from: 'planithelpdesk404@gmail.com',
          subject: 'Your password has been changed',
          text: 'Hello,\n\n' +
            'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          //req.flash('success', 'Success! Your password has been changed.');
          done(err);
        });
      }
    ], function(err) {
      res.redirect('/login');
    });
  });


module.exports = router;
