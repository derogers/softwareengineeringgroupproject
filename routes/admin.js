// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware");

router.get('/admin-management', middleware.checkAuth, async function (req, res){
    let tsk
    let allUsers = await User.find({}, function (err) {
        if (err) {
            console.log('User Fetch Error: ' + err)
        }
    }).exec()

    res.render('adminManagement', {
        ...middleware.buildCommonUserObject(req),
        allUsers: allUsers,
        taskStatus: {
            status: 'Waiting for task...'
        }
    })
})

router.post('/admin-task', middleware.checkAuth, async function (req, res){
    let taskStatus = {
        status: 'Task Error.  Try again later.'
    }

    if (req.body.actionTask === 'List Events and Lesson Plans') {
        console.log('--- Admin: List Events ---')
        let allEvents = await CalendarEvent.find({
            username: req.body.actionUser,
        }, function (err) {
            if (err) {
                console.log('Admin Events Fetch Error: ' + err)
            }
        }).sort({start: 'asc'}).exec()
        taskStatus.status = 'Fetched all user calendar events'
        taskStatus.data = allEvents
    } 
    else if (req.body.actionTask === 'List User Details') {
        console.log('--- Admin: List User Details ---')
        let userDetails = await User.find({
            username: req.body.actionUser,
        }, function (err) {
            if (err) {
                console.log('Admin Fetch User Details Error: ' + err)
            }
        }).exec()
        taskStatus.status = 'Fetched user details'
        taskStatus.data = userDetails
    }
    else if (req.body.actionTask === 'List Todos') {
        console.log('--- Admin: List Todos ---')
        let allTodos = await Todo.find({
            username: req.body.actionUser,
        }, function (err) {
            if (err) {
                console.log('Admin Todos Fetch Error: ' + err)
            }
        }).sort({start: 'asc'}).exec()
        taskStatus.status = 'Fetched all user todo items'
        taskStatus.data = allTodos
    }
    else if (req.body.actionTask === 'Delete User (CAREFUL)') {
        console.log('--- Admin: Delete User ---')
        await User.findOneAndDelete(
            { username: req.body.actionUser }, function (err, doc) {
                if (err) {
                    console.log('Admin: Error deleting user: ' + err)
                    taskStatus.status = 'Error deleting user'
                } else {
                    console.log('Admin: Deleted User: ' + doc)
                    taskStatus.status = 'Deleted user.'
                }
            }
        )
    }
    else if (req.body.actionTask === 'Delete User and purge all user data (CAREFUL)') {
        console.log('--- Admin: Delete User and Purge Data ---')
        await User.findOneAndDelete(
            { username: req.body.actionUser }, function (err, doc) {
                if (err) {
                    console.log('Admin: Error deleting user: ' + err)
                } else {
                    console.log('Admin: Deleted User: ' + doc)
                }
            }
        )
        await CalendarEvent.deleteMany(
            { username: req.body.actionUser }, function (err, doc) {
                if (err) {
                    console.log('Admin: Error deleting user event data: ' + err)
                } else {
                    console.log('Admin: Deleted events: ' + doc)
                }
            }
        )
        await Todo.deleteMany(
            { username: req.body.actionUser }, function (err, doc) {
                if (err) {
                    console.log('Admin: Error deleting user todo data: ' + err)
                } else {
                    console.log('Admin: Deleted todos: ' + doc)
                }
            }
        )
        taskStatus.status = 'Deleted user, event, todo data'
    }
    
    let allUsers = await User.find({}, function (err) {
        if (err) {
            console.log('User Fetch Error: ' + err)
        }
    }).exec()

    res.render('adminManagement', {
        ...middleware.buildCommonUserObject(req),
        allUsers: allUsers,
        taskStatus: taskStatus
    })
})

module.exports = router;