// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('../models/user')
CalendarEvent = require('../models/calendarEvent')
Todo = require('../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("../middleware");

var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var bcrypt = require('bcrypt-nodejs');


router.get('/confirmEmail', function(req, res) {
    res.render('confirmEmail', {
      user: req.user
    });
  });

router.get('/forgot', function(req, res) {
    res.render('forgot', {
      user: req.user
    });
  });

  router.post('/forgot', function(req, res, next) {
    async.waterfall([
      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          done(err, token);
        });
      },
      function(token, done) {
        User.findOne({ email: req.body.email }, function(err, user) {
          if (!user) {
            //req.flash('error', 'No account with that email address exists.');
            return res.redirect('/forgot');
          }

          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

          user.save(function(err) {
            done(err, token, user);
          });
        });
      },
      function(token, user, done) {
        var smtpTransport = nodemailer.createTransport({
          service: 'gmail',
          auth: {
              user: 'planithelpdesk404@gmail.com',
              pass: 'planitearth'
          }
        });
        var mailOptions = {
          to: user.email,
          from: 'planithelpdesk404@gmail.com',
          subject: 'Planned Password Reset',
          text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            'http://' + req.headers.host + '/reset/' + token + '\n\n' +
            'If you did not request this, please ignore this email and your password will remain unchanged.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
         // req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
          done(err, 'done');
        });
      }
    ], function(err) {
      if (err) return next(err);
      res.redirect('/confirmEmail');
    });
  });

  module.exports = router;
