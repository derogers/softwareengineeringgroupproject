// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
ath = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware");


// TODO LIST ---------------------------------------------------------------------------

// create todo
router.post('/create-todo', middleware.checkAuth, function (req, res){
    console.log('Received todo create request for item: ' + req.body.todo_item)

    const newTodo = new Todo({
        username: req.user.username,
        state: 'new',
        info: req.body.todo_item
    })

    newTodo.save(function (err){
        if (err) {
            console.log('Error creating a todo: ' + err)
            res.sendStatus(500)
        } else {
            console.log('Todo Created')
            res.status(200).send(newTodo)
        }
    })
})

// delete todo
router.delete('/delete-todo', middleware.checkAuth, function (req, res){
    console.log('Received todo delete request for item: ' + req.body.item)

    Todo.findOneAndDelete(
        { _id: new mongoose.Types.ObjectId(req.body.item) }, function (err, doc) {
            if (err) {
                console.log('Error deleting a todo: ' + err)
                res.sendStatus(500)
            } else {
                console.log('Deleted Todo Item: ' + doc)
                res.sendStatus(200)
            }
        }
    )
})



module.exports = router;