// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
path = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware")
var nodemailer = require('nodemailer');

 //router for registering and logging in and out, dashboard and profile page

router.get('/register', function (req, res){
    res.render('register.ejs')
})

router.post('/register', function (req, res){
    User.register(new User(
        {
            username: req.body.username,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            role: req.body.role,
            zip: req.body.zip
        }), req.body.password, function (err, user){
        if (err){
            console.log(err)
            return res.render('register.ejs',
                {
                    status: err.message
                })
        }
        passport.authenticate('local')(req, res, function (){
            var smtpTransport = nodemailer.createTransport({
                service: 'Gmail', 
                auth: {
                  user: 'planithelpdesk404@gmail.com',
                  pass: "planitearth"
                }
              });
              var mailOptions = {
                to: user.email,
                from: 'planithelpdesk404@gmail.com',
                subject: 'Thanks for Registering!',
                text: 'You have successfully created an account with Planned.\n\n'                                  
              };
              smtpTransport.sendMail(mailOptions, function(err) {
                console.log('mail sent');
              });

            if (req.user.role === 'principal') {
                if (req.session.redirectRoute) {
                    res.redirect(req.session.redirectRoute)
                } else {
                    res.redirect('/dashboard-principal')
                }
            } else if (req.user.role === 'teacher') {
                if (req.session.redirectRoute) {
                    res.redirect(req.session.redirectRoute)
                } else {
                    res.redirect('/dashboard')
                }
            } else if (req.user.role === 'admin') {
                if (req.session.redirectRoute) {
                    res.redirect(req.session.redirectRoute)
                } else {
                    res.redirect('/dashboard-admin')
                }
            }   
        })
    })
})

 router.get('/login', function (req, res){
    res.render('login')
})

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) {
        return next(err); // will generate a 500 error
      }
      if (!user) {
        return res.render('login.ejs',
                {
                    status: "Incorrect login"
                })
      }
      req.login(user, function(err){
        if(err){
          return next(err);
        }
        if (req.user.role === 'principal') {
            if (req.session.redirectRoute) {
                res.redirect(req.session.redirectRoute)
            } else {
                res.redirect('/dashboard-principal')
            }
        } else if (req.user.role === 'teacher') {
            if (req.session.redirectRoute) {
                res.redirect(req.session.redirectRoute)
            } else {
                res.redirect('/dashboard')
            }
        } else if (req.user.role === 'admin') {
            if (req.session.redirectRoute) {
                res.redirect(req.session.redirectRoute)
            } else {
                res.redirect('/dashboard-admin')
            }
        }       
      });
    })(req, res, next);
  });

router.get('/logout', function (req, res){
    
    req.session.redirectRoute = null
    req.logout()
    res.redirect('/')
})




module.exports = router;