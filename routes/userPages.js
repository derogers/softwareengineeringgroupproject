// DASHBOARD ---------------------------------------------------------------------------

// Dependency Setup
const express = require('express')
mongoose = require('mongoose')
passport = require('passport')
path = require('path')
router = express.Router()
bodyParser = require('body-parser')
User = require('./../models/user')
CalendarEvent = require('./../models/calendarEvent')
Todo = require('./../models/todo')
LocalStrategy = require('passport-local')
// const moment = require('moment')
moment = require('moment-timezone')
middleware = require("./../middleware");
 
router.get('/dashboard', middleware.checkAuth, async function(req, res) {
    const classEvents = await CalendarEvent.find({
        username: req.user.username,
        start: {
            $gte: moment().startOf('day'),
            $lt:  moment().endOf('day')  
        },
        type: "Class"
    }, function (err) {
        if (err) {
            console.log('Dashboard Class Events Error: ' + err)
        }
    }).sort({start: 'asc'}).exec()

    const otherEvents = await CalendarEvent.find({
        username: req.user.username,
        start: {
            $gte: moment().startOf('day'),
            $lt:  moment().endOf('day')  
        },
        type: "Other"
    }, function (err) {
        if (err) {
            console.log('Dashboard Other Events Error: ' + err)
        }
    }).sort({start: 'asc'}).exec()

    const pendingPlans = await CalendarEvent.find({
        username: req.user.username,
        type: "Class",
        'lesson.state': 'pending'
    }, function (err) {
        if (err) {
            console.log('Dashboard Pending Plans Error: ' + err)
        }
    }).sort({start: 'asc'}).exec()
    
    const todosReceived = await Todo.find({
        username: req.user.username
    }, function (err) {
        if (err) {
            console.log('Dashboard Todo Error: ' + err)
        }
    }).exec()

    res.render('dashboard',
        {
            ...middleware.buildCommonUserObject(req),
            calendarEvents: {
                class: classEvents,
                other: otherEvents,
                plans: pendingPlans
            },
            todos: todosReceived
        })
})

// principal dashboard
router.get('/dashboard-principal', middleware.checkAuth, async function (req, res){
    const pendingPlans = await CalendarEvent.find({
        type: "Class",
        'lesson.state': 'pending'
    }, function (err) {
        if (err) {
            console.log('Dashboard Pending Plans Error: ' + err)
        }
    }).sort({start: 'asc'}).exec()
    
    const todosReceived = await Todo.find({
        username: req.user.username
    }, function (err) {
        if (err) {
            console.log('Dashboard Todo Error: ' + err)
        }
    }).exec()

    res.render('dashboardPrincipal',
        {
            ...middleware.buildCommonUserObject(req),
            calendarEvents: {
                plans: pendingPlans
            },
            todos: todosReceived
        })
})

// principal dashboard
router.get('/dashboard-admin', middleware.checkAuth, async function (req, res){
    
    const todosReceived = await Todo.find({
        username: req.user.username
    }, function (err) {
        if (err) {
            console.log('Dashboard Todo Error: ' + err)
        }
    }).exec()

    res.render('dashboardAdmin',
        {
            ...middleware.buildCommonUserObject(req),
            todos: todosReceived
        })
})

// FAQ ---------------------------------------------------------------------------

// faq page
router.get('/faq', function (req, res){
    res.render('faq.ejs', { ...middleware.buildCommonUserObject(req) })
})



// PROFILE ---------------------------------------------------------------------------

// profile page
router.get('/profile', middleware.checkAuth, function (req, res){
    res.render('profile.ejs', { ...middleware.buildCommonUserObject(req) })
})

// edit profile page
router.get('/editprofile', middleware.checkAuth, function (req, res){
    res.render('editProfile.ejs', { ...middleware.buildCommonUserObject(req) })
})

// save edited profile info
router.post('/editprofile', middleware.checkAuth, function (req, res, next) {
    User.findOneAndUpdate(
        { _id: new mongoose.Types.ObjectId(req.user.id) }, {
            $set: {
                email: req.body.userEmail,
                firstname: req.body.firstName,
                lastname: req.body.lastName,
                zip: req.body.userZip
            }
        }, function (err, doc) {
            if (err) {
                console.log('Profile edit error: ' + err)
            } else {
                console.log('Updated profile: ' + doc)
            }
        }
    )
    res.redirect('/profile')
})


module.exports = router;