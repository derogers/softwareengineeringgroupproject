// Dependency Setup
const express = require('express')
const mongoose = require('mongoose')
const passport = require('passport')
const path = require('path')
const bodyParser = require('body-parser')
const User = require('./models/user')
const CalendarEvent = require('./models/calendarEvent')
const Todo = require('./models/todo')
const LocalStrategy = require('passport-local')
// const moment = require('moment')
const moment = require('moment-timezone')


// Initialize app
const app = express()

// Connect to Mongo Database
const mongoUrl = 'mongodb+srv://dbuser:command10@lessonsdb-da8ym.mongodb.net/test?retryWrites=true&w=majority'
mongoose.connect(mongoUrl, {
    reconnectTries: 100,
    reconnectInterval: 500,
    autoReconnect: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    dbName: 'test'
})
    .catch(err => console.log('Mongo connection error', err))
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)

// App setup
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(require('express-session')({
    secret: 'keep-it-secret-keep-it-safe',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 30 * 24 * 60 * 60 * 1000
    }
}))
app.use(express.static(path.join(__dirname, '/public')))
app.use(passport.initialize())
app.use(passport.session())

// Passport Setup
passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())


// ------------------------ Route Setup ------------------------

var authenticateRoutes = require("./routes/authenticate")
var eventRoutes = require("./routes/events")
var todoListRoutes = require("./routes/todoList")
var userPagesRoutes = require("./routes/userPages")
var indexPageRoutes = require("./routes/indexPage")
var forgotRoutes = require("./routes/passwordForgot")
var resetRoutes = require("./routes/passwordReset")
var adminRoutes = require("./routes/admin")

app.use(authenticateRoutes);
app.use(eventRoutes);
app.use(todoListRoutes);
app.use(userPagesRoutes);
app.use(indexPageRoutes);
app.use(resetRoutes);
app.use(forgotRoutes);
app.use(adminRoutes);

// SERVER START ---------------------------------------------------------------------------

// listen
app.listen(8080, process.env.IP, function (){
    console.log('Server started.  Please visit: 127.0.0.1:8080')
})
