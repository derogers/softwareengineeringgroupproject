const mongoose = require('mongoose')
const Schema = mongoose.Schema
const passportLocalMongoose = require('passport-local-mongoose')

var LessonPlan = new Schema({
    objective: { type: String, default: ''},
    lesson: { type: String, default: ''},
    assessment: { type: String, default: ''},
    state: { type: String, default: 'new'}
 });

var CalendarEvent = new Schema({
    title: String,
    type: String,
    start: Date,
    end: Date,
    description: String,
    username: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    allDay: { type: Boolean, default: false },
    className: String,
    lesson: LessonPlan
})

CalendarEvent.plugin(passportLocalMongoose)

module.exports = mongoose.model('CalendarEvent', CalendarEvent)