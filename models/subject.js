const mongoose = require('mongoose')
const Schema = mongoose.Schema
const passportLocalMongoose = require('passport-local-mongoose')

var Subject = new Schema({
    name: String,
    schedule: [],
    username: { type: String, required: true },
    createdAt: { type: Date, default: Date.now }
})

Subject.plugin(passportLocalMongoose)

module.exports = mongoose.model('Subject', Subject)
