const mongoose = require('mongoose')
const Schema = mongoose.Schema
const passportLocalMongoose = require('passport-local-mongoose')

const User = new Schema({
    username: { type: String, required: true, unique: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    role: { type: String, required: true },
    email: { type: String, required: true },
    zip: { type: String, requireed: true },
    resetPasswordToken: String,
    resetPasswordExpires: Date
})


User.plugin(passportLocalMongoose)

module.exports = mongoose.model('User', User)
